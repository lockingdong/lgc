<?php include 'includes/header.php'; ?>


<section id="order-list" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>Order</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="order-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>訂單</div>
					</div>	
				</div>
			</div>
			<div class="list-title">
				<div class="row">
					<div class="col-3">訂單編號</div>
					<div class="col-2">訂單日期</div>
					<div class="col-2">價錢</div>
					<div class="col-2">狀態</div>
					<div class="col-3"></div>
				</div>	
			</div>
			<div class="list-wrap">
				<div class="list">
					<div class="row">
						<div class="col-12 col-lg-3">
							<span class="item-title">訂單編號&nbsp</span><span>2017356894030246</span>
						</div>
						<div class="col-12 col-lg-2">
							<span class="item-title">訂單日期&nbsp</span><span>2017-12-31</span>
						</div>
						<div class="col-12 col-lg-2">
							<span class="item-title">價錢&nbsp</span><span>NT$1,200</span>
						</div>
						<div class="col-12 col-lg-3">
							<span class="item-title">狀態&nbsp</span><span>訂單處理中</span>
						</div>
						<div class="col-12 col-lg-2">
							<a href="order-atm.php">
								<button>訂單詳情</button>
							</a>
						</div>
					</div>	
				</div>
				<?php
					$data = array("訂單處理中", "已完成", "已取消", "已退款", "已完成", "已取消");
					$i=0; 
					while($i<5): 
				?>
				<div class="list">
					<div class="row">
						<div class="col-12 col-lg-3">
							<span class="item-title">訂單編號&nbsp</span><span>2017356894030246</span>
						</div>
						<div class="col-12 col-lg-2">
							<span class="item-title">訂單日期&nbsp</span><span>2017-12-31</span>
						</div>
						<div class="col-12 col-lg-2">
							<span class="item-title">價錢&nbsp</span><span>NT$1,200</span>
						</div>
						<div class="col-12 col-lg-3">
							<span class="item-title">狀態&nbsp</span><span><?php echo $data[$i]; ?></span>
						</div>
						<div class="col-12 col-lg-2">
							<a href="order-711.php">
								<button>訂單詳情</button>
							</a>
						</div>
					</div>	
				</div>
				<?php 
					$i++;
					endwhile; 
				?>
			</div>
			
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="pagination">
						<ul>
							<li><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>	
	</div>
</section>




<?php include 'includes/footer.php'; ?>