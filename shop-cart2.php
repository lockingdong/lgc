<?php include 'includes/header.php'; ?>

<section id="shop-cart" class="cart2">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>Shopping cart</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="cart-form-wrap">
		<form action="">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="title green-decor-title">
							<div class="decor-squ"></div>
							<div>訂購商品</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="list-bottom">
							<div class="title-wrap">
								<div class="col-12 col-lg-4">品名</div>
								<div class="col-12 col-lg-2">價格</div>
								<div class="col-12 col-lg-2">數量</div>
								<div class="col-12 col-lg-2">小計</div>
								<!-- <div class="col-12 col-lg-2">取消</div> -->
							</div>

							<?php 
								$i = 0;
								while($i<2):
							 ?>
							<div class="list-wrap">
								<div class="col-12 col-lg-4">
									<div class="list-inner-title">品名</div>
									<div class="product-name-wrap flex-wraper">
										<div class="img-wrap">
											<img src="img/cart/images.jpg" alt="">
										</div>
										<div class="text-wrap">
											
											<div class="product-name">極淨洗顏霜</div>
											<div class="sub-text">顏色：白色</div>
											<div class="sub-text">容量：150ml</div>
										</div>
									</div>
								</div>
								<div class="col-12 col-lg-2">
									<div class="list-inner-title">價格</div>
									<div class="flex-wraper">
										NT$399
									</div>
								</div>
								<div class="col-12 col-lg-2">
									<div class="list-inner-title">數量</div>
									<div class="flex-wraper">
										<div class="input-wrap">
											<input min="0" type="number" class="disable" value="2" disabled>
										</div>
										
									</div>
								</div>
								<div class="col-12 col-lg-2">
									<div class="list-inner-title">小計</div>
									<div class="flex-wraper">
										NT$798
									</div>
								</div>
								<div class="col-12 col-lg-2">
									<!-- <div class="list-inner-title">取消</div>
									<div class="flex-wraper">
										<button class="fa fa-trash-o delete-btn"></button>
									</div> -->
								</div>
							</div>
							
							<?php
								$i++;
								endwhile;
							 ?>


							<div class="total-wrap">
								<div class="col-8 col-lg-4 offset-4 offset-lg-6">
									<div class="item-wrap">
										<div class="label">合計</div>
										<div class="price">NT$1586</div>
									</div>
									<div class="item-wrap">
										<div class="label">運費</div>
										<div class="price">NT$100</div>
									</div>
									<div class="item-wrap">
										<div class="label main-label">總計</div>
										<div class="price main-total">NT$1686</div>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>


			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="title green-decor-title">
							<div class="decor-squ"></div>
							<div>配送方式及付款方式</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="list-bottom shipping-radio-area cart2">

							<div class="radio-area">


								<!-- 這裡面的東西 我用css讓他不能點選 -->


								<!-- 3選1 -->
								<div class="radio-wrap">
									<label>
										<div class="img-wrap">
											<img src="img/cart/svg/icon-01.svg" alt="">
											<div class="text">郵局寄送</div>
										</div>
										<input type="radio" name="shipping-methods" value="post" checked disabled>
									</label>
								</div>
								<!--  -->

								<!--  -->
								<div class="radio-wrap">
									<label>
										<div class="img-wrap">
											<img src="img/cart/svg/icon-02.svg" alt="">
											<div class="text">超商取貨</div>
										</div>
										<input type="radio" name="shipping-methods" value="store" disabled>
									</label>
								</div>
								<!--  -->

								<!--  -->
								<div class="radio-wrap">
									<label>
										<div class="img-wrap">
											<img src="img/cart/svg/icon-04.svg" alt="">
											<div class="text">黑貓宅急</div>
										</div>
										<input type="radio" name="shipping-methods" value="cat" disabled>
									</label>
								</div>
								<!--  -->

								<!-- 4選1 -->
								<div class="radio-wrap">
									<label>
										<div class="img-wrap">
											<img src="img/cart/svg/icon-05.svg" alt="">
											<div class="text">信用卡</div>
										</div>
										<input type="radio" name="payment-methods" value="" checked disabled>
									</label>
								</div>
								<!--  -->
								
								<!--  -->
								<div class="radio-wrap">
									<label>
										<div class="img-wrap">
											<img src="img/cart/svg/icon-02.svg" alt="">
											<div class="text">超商取貨付款</div>
										</div>
										<input type="radio" name="payment-methods" value="" disabled>
									</label>
								</div>
								<!--  -->

								<!--  -->
								<div class="radio-wrap">
									<label>
										<div class="img-wrap">
											<img src="img/cart/svg/icon-06.svg" alt="">
											<div class="text">ATM轉帳</div>
										</div>
										<input type="radio" name="payment-methods" value="" disabled>
									</label>
								</div>
								<!--  -->

								<!--  -->
								<div class="radio-wrap">
									<label>
										<div class="img-wrap">
											<img src="img/cart/svg/icon-07.svg" alt="">
											<div class="text">貨到付款</div>
										</div>
										<input type="radio" name="payment-methods" value="" disabled>
									</label>
								</div>
								<!--  -->

								
							</div>
							<div class="radio-textarea">
								<!-- <div class="radio-text-wrap">
									<p>
										1.到南京時，有朋友約去遊逛，勾留了一日；<br>
										2.第二日上午便須渡江到浦 口，下午上車北去。<br>3.父親因為事忙，本已說定不送我，叫旅館裡一個 熟識的茶房陪我同去。
									</p>
								</div>
								<div class="radio-text-wrap">
									<p>
										1.我們過了江，進了車站。我買票，他忙著照看行李。<br>
										2.行李太多了，得 向腳夫行些小費，才可過去。他便又忙著和他們講價錢。<br>
										3.我那時真是 聰明過分，總覺他說話不大漂亮，非自己插嘴不可。
									</p>
								</div>
								<div class="radio-text-wrap">
									<p>
										1.我看見他戴著黑布小帽，穿著黑布大馬褂，深青布棉袍<br>
										2.蹣跚地走到 鐵道邊，慢慢探身下去，尚不大難。<br>
										3.可是他穿過鐵道，要爬上那邊月 台，就不容易了。
									</p>
								</div> -->
							</div>

						</div>
					</div>
				</div>
			</div>



			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="title green-decor-title">
							<div class="decor-squ"></div>
							<div>訂購人</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="list-bottom info-bottom">
							<div class="row form-wrap">
								<div class="col-12 col-lg-6">
									<div class="item-wrap">
										<label for="">姓名</label>
										<div class="input-wrap">
											<input class="order-name" type="text" placeholder="輸入您的姓名" value="阿明" disabled>
										</div>
									</div>
								</div>
								<div class="col-12 col-lg-6">
									<div class="item-wrap">
										<label for="">電話</label>
										<div class="input-wrap">
											<input class="order-phone" type="text" placeholder="輸入您的聯絡電話" value="0999999999" disabled>
										</div>
									</div>
								</div>
								<div class="col-12 col-lg-6">
									<div class="item-wrap">
										<label for="">信箱</label>
										<div class="input-wrap">
											<input type="text" placeholder="輸入您的信箱" value="abc@gmail.com" disabled>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="title green-decor-title">
							<div class="decor-squ"></div>
							<div>收件人</div>
						</div>
					</div>
				</div>
				<div class="row">
					
					<div class="col-12">
						<div class="list-bottom info-bottom">
							<div class="row form-wrap">
								<div class="col-12">
									<div class="item-wrap">


										<!-- <div class="ui checkbox">
											<input type="checkbox" class="same-person" disabled>
											<label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp同訂購人打勾</label>
										</div> -->



									</div>
								</div>
								<div class="col-12 col-lg-6">
									<div class="item-wrap">
										<label for="">姓名</label>
										<div class="input-wrap">
											<input class="receiver-name" type="text" placeholder="輸入您的姓名" value="阿明" disabled >
										</div>
									</div>
								</div>
								<div class="col-12 col-lg-6">
									<div class="item-wrap">
										<label for="">手機號碼</label>
										<div class="input-wrap">
											<input class="receiver-phone" type="text" placeholder="輸入您的手機號碼" value="0999999999" disabled >
										</div>
									</div>
								</div>


								<!-- 看情況顯示 -->
								<div class="col-12 col-lg-6">
									<div class="item-wrap">
										<label for="">取貨門市</label>
										<div class="input-wrap">
											<input type="text" value="xxxx門市" disabled>
										</div>
									</div>
								</div>



								<div class="col-12">
									<div class="item-wrap address-wrap">
										<label for="">地址</label>
										<div class="input-wrap">
											<div class="select-wrap">
												<select class="receiver-city" disabled>
													<option value="縣市" disabled>縣市</option>
													<option value="台北市" selected>台北市</option>
												</select>
											</div>
											<div class="select-wrap">
												<select class="receiver-county" disabled>
													<option value="鄉鎮市區" disabled>鄉鎮市區</option>
													<option value="中山區" selected>中山區</option>
												</select>
											</div>
											<div class="label">
												<input class="receiver-zip" max="3" type="text" placeholder="XXX" value="105" disabled>
											</div>
											<div class="address-input">
												<input class="receiver-address" type="text" placeholder="輸入您的地址" value="長安東路100號100樓" disabled>
											</div>
										</div>
									</div>
								</div>
								<div class="col-12">
									<div class="item-wrap textarea-wrap">
										<label for="">訂購備註</label>
										<div class="input-wrap">
											<textarea placeholder="輸入內容" rows="6" disabled>備註內容備註內容備註內容備註內容備註內容備註內容備註內容備註內容</textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>



			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="title green-decor-title">
							<div class="decor-squ"></div>
							<div>發票</div>
						</div>
					</div>
				</div>
				<div class="row">
					
					<div class="col-12">
						<div class="list-bottom info-bottom">
							<div class="row form-wrap">
								
								
								
								<!-- 形式1 -->
								<div class="col-12">
									<div class="item-wrap">
										<div class="ui radio checkbox">
											<input type="radio" name="invoice" checked="checked" value="two" disabled>
											<label>二連發票</label>
										</div>
									</div>
									<div class="item-wrap address-wrap for-two-invoice">
										<label for="">地址</label>
										<div class="input-wrap">
											<div class="select-wrap">
												<select class="invoice-city" disabled>
													<option value="縣市" selected="" disabled>縣市</option>
													<option value="台北市">台北市</option>
												</select>
											</div>
											<div class="select-wrap">
												<select class="invoice-county" disabled>
													<option value="鄉鎮市區" selected="" disabled>鄉鎮市區</option>
													<option value="中山區">中山區</option>
												</select>
											</div>
											<div class="label">
												<input class="invoice-zip" type="text" max="3" placeholder="XXX" disabled>
											</div>
											<div class="address-input">
												<input class="invoice-address" type="text" placeholder="輸入您的地址" disabled>
											</div>
										</div>
									</div>
								</div>






								<!-- 形式2 -->
								<div class="col-12">
									<div class="item-wrap">
										<div class="ui radio checkbox">
											<input type="radio" name="invoice"  value="three" disabled>
											<label>三聯發票</label>
										</div>
									</div>
									<div class="item-wrap">
										<label for="">統一編號</label>
										<div class="input-wrap">
											<input type="text" placeholder="" value="xxx公司" disabled>
										</div>
									</div>
									<div class="item-wrap">
										<label for="">發票抬頭</label>
										<div class="input-wrap">
											<input type="text" placeholder="" value="12345678" disabled>
										</div>
									</div>
									<div class="item-wrap address-wrap for-two-invoice">
										<label for="">地址</label>
										<div class="input-wrap">
											<div class="select-wrap">
												<select class="invoice-city" disabled>
													<option value="縣市" selected="" disabled>縣市</option>
													<option value="台北市">台北市</option>
												</select>
											</div>
											<div class="select-wrap">
												<select class="invoice-county" disabled>
													<option value="鄉鎮市區" selected="" disabled>鄉鎮市區</option>
													<option value="中山區">中山區</option>
												</select>
											</div>
											<div class="label">
												<input class="invoice-zip" type="text" max="3" placeholder="XXX" disabled>
											</div>
											<div class="address-input">
												<input class="invoice-address" type="text" placeholder="輸入您的地址" disabled>
											</div>
										</div>
									</div>
								</div>





								<!-- 形式3 -->
								<div class="col-12">
									<div class="item-wrap">
										<div class="ui radio checkbox">
											<input type="radio" name="invoice"  value="don" disabled>
											<label>捐贈發票</label>
										</div>
									</div>
									<div class="item-wrap">
										<p>
											「我買幾個橘子 去。你就在此地，不要走動。」我看那邊月台的柵欄外有幾個賣東西 的等著顧客。走到那邊月台，須穿過鐵道，須跳下去又爬上去。
											「我買幾個橘子 去。你就在此地，不要走動。」我看那邊月台的柵欄外有幾個賣東西 的等著顧客。走到那邊月台，須穿過鐵道，須跳下去又爬上去。
										</p>
									</div>
								</div>
								
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="btn-area apply-btn-area">
					<button type="submit">確認訂購</button>
				</div>
			</div>
			
		</form>
	</div>
</section>





<?php include 'includes/footer.php'; ?>

<script>
	// $( ".spinner" ).spinner();

	// $(".ui-spinner-button.ui-spinner-up").html(`<div class='arr'>▲</div>`)
	// $(".ui-spinner-button.ui-spinner-down").html(`<div class='arr'>▼</div>`)
	
</script>