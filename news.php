<?php include 'includes/header.php'; ?>

<section id="news" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>News</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="white-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>最新消息</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php 
					$i = 0;
					while($i<5):
				?>
				<div class="col-12">
					<a href="news-inner.php">
						<div class="news-wrap">
							<div class="date">2018-01-30</div>
							<div class="excerpt">
								過鐵道時，他先將橘子散放在地上，自己慢 慢爬下，再抱起橘子走。到這邊時，我趕緊去攙他。他和我走到車上 ，將橘子一股腦兒放在我的皮大衣上。於是拍拍衣上泥土，心裡很輕 鬆似的。
							</div>
						</div>
					</a>
				</div>
				<?php
					$i++; 
					endwhile; 
				?>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="pagination">
						<ul>
							<li><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<?php include 'includes/footer.php'; ?>