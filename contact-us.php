<?php include 'includes/header.php'; ?>

<section id="contact-us" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>Contact Us</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom-wrap">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="content-wrap">
						<div class="title green-decor-title">
							<div class="decor-squ"></div>
							<div>Line</div>
						</div>
						<div class="content">
							<p>內文內文內文內文內文內文內文內文 LINE ID: wsx1447t</p>
							<div class="line-wrap">
								<a href="line://ti/p/@wxs1447t">
									<img src="img/svg/line-f.svg" alt="">	
								</a>
								
							</div>
							<p>內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文內文<br>
								 內文內文內文內文內文內文內文
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="content-wrap">
						<div class="title green-decor-title">
							<div class="decor-squ"></div>
							<div>聯絡我們</div>
						</div>
						<div class="form-wrap">
							<form action="" id="contact-us-form">
								<div class="row">
									<div class="col-12 col-lg-6">
										<div class="input-wrap">
											<label for="">姓名</label>
											<input name="client-name" type="text" placeholder="輸入您的姓名">
										</div>
									</div>
									<div class="col-12 col-lg-6">
										<div class="input-wrap">
											<label for="">電話</label>
											<input name="phone" type="text" placeholder="輸入您的電話">
										</div>
									</div>
									<div class="col-12">
										<div class="input-wrap">
											<label for="">電子信箱</label>
											<input name="email" type="text" placeholder="輸入您的EMAIL">
										</div>
									</div>
									<div class="col-12">
										<div class="input-wrap">
											<label for="">標題</label>
											<input type="text" placeholder="輸入標題">
										</div>
									</div>
									<div class="col-12">
										<div class="input-wrap textarea">
											<label for="">內容</label>
											<textarea name="content" placeholder="輸入內容" name="" id="" cols="30" rows="10"></textarea>
										</div>
									</div>
									<div class="col-12">
										<div class="btn-area">
											<input type="button" value="清除" onclick="document.getElementById('contact-us-form').reset();">
											<input type="submit" value="傳送">
										</div>
									</div>
								</div>
							</form>
						</div>
							
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<?php include 'includes/footer.php'; ?>