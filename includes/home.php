<div class="container-fluid leave-top" style="overflow: hidden;">
	<!-- bannerSlick -->
	<div class="row indexBn-box">
		<ul class='indexBn-slick'>
			<?php
				$b_urls = ['img/banner/index-bn1.jpg', 'img/banner/BANNER_02.png', 'img/banner/BANNER_03.png']; 
			 ?>
			<?php for($i=0;$i<count($b_urls);$i++){ ?>
			<li class='bn' >
				<div class="bg-wrap" style="background-image: url(<?php echo $b_urls[$i] ;?>)"></div>
				<div class="container-fluid text-container">
					<div class="row bn-row">
						<div class="bn-text1">
							<h2 class='green'>主打商品1主標副標</h2>
							<p class='grey9a'>液狀質地滲透肌底液狀質地滲透肌底液狀質地滲透肌底液狀質地滲透肌底</p>
						</div>
					</div>
				</div>
			</li>
			<?php } ?>
			<!-- <li class='bn' style="background-image: url('img/banner/index-bn1.jpg')">
			</li> -->
		</ul>
		<div class="arrow-control">
			<a href="" class='left'><img src="img/svg/banner_arrow_l.svg" alt=""></a>
			<a href="" class='right'><img src="img/svg/banner_arrow_r.svg" alt=""></a>
		</div>
	</div>
	
	<!-- slick1 -->
	<div class="row mb60">
		<div class="container">
			<div class="row">
				<div class="w-100">
					<h2 class='text-center lobster-font font40'>New Products</h2>
				</div>
			</div>
			<div class="index-p1-out">
				
				<div class="row index-p1">
					
					<?php for($i=0;$i<4;$i++) { ?>
					<div class="col-6 col-md-4 ">
						<div class="ip-box">
							<a href="product-inner.php">
								<figure>
									<img src="img/png/p<?php echo $i+1 ;?>.jpg" class='img-rwd'>
									<?php if($i%3 == 0) { ?>
									<figcaption><p>限時<br>特價</p><span></span></figcaption>
									<?php } ?>
								</figure>
							</a>
							<h4 class='ip-name'>Cardamom 洗面乳</h4>
							<p class='ip-price-onsale'>特價：1200</p>
							<STRIKE class='ip-price'>原價：999</STRIKE>
							<a href="" class='ip-cart'>加入購物車</a>
						</div>
					</div>
					<?php } ?>
					

				</div>
				<div class="index-control ic1">
					<a class="left"> 
						<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  viewBox="0 0 42 24" style="enable-background:new 0 0 42 24;" xml:space="preserve">
						
						<path d="M0,12l12,12h6L6,12L18,0l-6,0L0,12z M12,12l12,12h6L18,12L30,0l-6,0L12,12z M24,12l12,12h6L30,12L42,0l-6,0
							L24,12z"/>
						</svg>
					</a>
					<a class="right">
						<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 42 24" style="enable-background:new 0 0 42 24;" xml:space="preserve">
						<path d="M30,0h-6l12,12L24,24h6l12-12L30,0z M18,0h-6l12,12L12,24h6l12-12L18,0z M6,0H0l12,12L0,24h6l12-12L6,0z"/>
						</svg>
					</a>
				</div>
				
			</div>

			<!-- see more start -->
			<div class="row">
				<div class="col-12">
					<a href='product-new.php' class='seemore'>
						<svg version="1.1" id="圖層_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								viewBox="0 0 19 19" style="enable-background:new 0 0 19 19;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#231815;}
							.st1{fill:#FFFFFF;}
						</style>
						<g>
							<circle class="st0" cx="9.5" cy="9.5" r="9.5"/>
							<path class="st1" d="M15.7,10.7l-5,5c-0.6,0.6-1.7,0.6-2.4,0c-0.6-0.6-0.6-1.7,0-2.3c0.3-0.3,2.1-2.2,2.1-2.2h-6
								c-0.9,0-1.7-0.7-1.7-1.7s0.7-1.7,1.7-1.7h6c0,0-1.8-1.8-2.1-2.1C7.7,5,7.7,4,8.3,3.3s1.7-0.6,2.4,0l5,5C16.3,9,16.3,10,15.7,10.7z"
								/>
						</g>
						</svg>
						SEE MORE
					</a>
				</div>
			</div>
			<!-- see more end -->
		</div>
	</div>

	<!-- banner1 -->
	<div class="row mb60 index-bn2 banner2" onclick="window.location.href='product-inner.php'" >
		<div class="bg-wrap" style="background-image: url('img/png/activity_01.jpg');"></div>
		<div class="container-fluid" style="position: relative;">
			<div class="row">
				<div class="bn-text1">
					<h2 class='green'>主打商品1主標副標</h2>
					<p class='black'>液狀質地滲透肌底</p>
				</div>
			</div>
		</div>
	</div>

	<!-- slick2 -->
	<div class="row mb60">
		<div class="container">
			<div class="row">
				<div class="w-100">
					<h2 class='text-center lobster-font font40'>Popular Products</h2>
				</div>
			</div>
			<div class="index-p2-out">
				
				<div class="row index-p1">
					
					<?php for($i=0;$i<5;$i++) { ?>
					<div class="col-6 col-md-3">
						<div class="ip-box">
							<a href="product-inner.php">
								<figure>
									<img src="img/png/p<?php echo $i%4+1 ;?>.jpg" class='img-rwd'>
									<?php if($i%3 == 0) { ?>
									<figcaption><p>限時<br>特價</p><span></span></figcaption>
									<?php } ?>
								</figure>
							</a>
							<h4 class='ip-name'>Cardamom 洗面乳</h4>
							<p class='ip-price-onsale'>特價：1200</p>
							<STRIKE class='ip-price'>原價：999</STRIKE>
							<a href="" class='ip-cart'>加入購物車</a>
						</div>
					</div>
					<?php } ?>

				</div>
				<div class="index-control ic2">
					<a class="left">
						<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  viewBox="0 0 42 24" style="enable-background:new 0 0 42 24;" xml:space="preserve">
						
						<path d="M0,12l12,12h6L6,12L18,0l-6,0L0,12z M12,12l12,12h6L18,12L30,0l-6,0L12,12z M24,12l12,12h6L30,12L42,0l-6,0
							L24,12z"/>
						</svg>
					</a>
					<a class="right">
						<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 42 24" style="enable-background:new 0 0 42 24;" xml:space="preserve">
						<path d="M30,0h-6l12,12L24,24h6l12-12L30,0z M18,0h-6l12,12L12,24h6l12-12L18,0z M6,0H0l12,12L0,24h6l12-12L6,0z"/>
						</svg>
					</a>
				</div>

				
			</div>
			<!-- see more start -->
			<div class="row">
				<div class="col-12">
					<a href='product-all.php' class='seemore'>
						<svg version="1.1" id="圖層_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								viewBox="0 0 19 19" style="enable-background:new 0 0 19 19;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#231815;}
							.st1{fill:#FFFFFF;}
						</style>
						<g>
							<circle class="st0" cx="9.5" cy="9.5" r="9.5"/>
							<path class="st1" d="M15.7,10.7l-5,5c-0.6,0.6-1.7,0.6-2.4,0c-0.6-0.6-0.6-1.7,0-2.3c0.3-0.3,2.1-2.2,2.1-2.2h-6
								c-0.9,0-1.7-0.7-1.7-1.7s0.7-1.7,1.7-1.7h6c0,0-1.8-1.8-2.1-2.1C7.7,5,7.7,4,8.3,3.3s1.7-0.6,2.4,0l5,5C16.3,9,16.3,10,15.7,10.7z"
								/>
						</g>
						</svg>
						SEE MORE
					</a>
				</div>
			</div>
			<!-- see more end -->
		</div>
	</div>

	<!-- banner2 -->
	<div class="row index-bn2 banner3" onclick="window.location.href='product-inner.php'" >
		<div class="bg-wrap" style="background-image: url('img/png/activity_02.jpg')"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="bn-text1">
					<h2 class='black'>主打商品1主標副標</h2>
					<p class='green'>液狀質地滲透肌底液狀質地滲透肌底液狀質地滲透肌底液狀質地滲透肌底</p>
				</div>
			</div>
		</div>
	</div>
	

	<!-- article -->
	<div class="row index-article-box">
		<div class="container">
			<div class="row">
				<div class="w-100">
					<h2 class='text-center lobster-font font40'>Combination Products</h2>
				</div>
			</div>
			<?php for($i=0;$i<2;$i++){ ?>
			<div class="col-12">
				<div class="row index-article">
					<div class="col-12 col-lg-7 image" style="background-image: url('img/png/c_00<?php echo $i%2+1; ?>.jpg');">
					</div>
					<div class="col-12 col-lg-5 intro">
						<h3>華麗美顏組合</h3>
						<p class="price">特價：2399元</p>
						<p class='info'>
							．液狀質地滲透洗面乳<br>
							．柔皙美肌隔離乳<br>
							．淨白面膜<br>
							．光彩精華液<br>
							．保濕化妝水
						</p>
						<a href="mix-product-inner.php" class='link'> 
							<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 19 19" style="enable-background:new 0 0 19 19;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#231815;}
								.st1{fill:#FFFFFF;}
							</style>
							<g>
								<circle class="st0" cx="9.5" cy="9.5" r="9.5"/>
								<path class="st1" d="M15.7,10.7l-5,5c-0.6,0.6-1.7,0.6-2.4,0c-0.6-0.6-0.6-1.7,0-2.3c0.3-0.3,2.1-2.2,2.1-2.2h-6
									c-0.9,0-1.7-0.7-1.7-1.7s0.7-1.7,1.7-1.7h6c0,0-1.8-1.8-2.1-2.1C7.7,5,7.7,4,8.3,3.3s1.7-0.6,2.4,0l5,5C16.3,9,16.3,10,15.7,10.7z"
									/>
							</g>
							</svg>SEE MORE
						</a>
					</div>
				</div>
			</div>

			<div class="col-12">
				<div class="row index-article">
					<div class="col-12 col-lg-7  push-lg-5 image" style="background-image: url('img/png/c_00<?php echo $i%2+1; ?>.jpg');">
					</div>
					<div class="col-12 col-lg-5  pull-lg-7 intro">
						<h3>華麗美顏組合</h3>
						<p class="price">特價：2399元</p>
						<p class='info'>
							．液狀質地滲透洗面乳<br>
							．柔皙美肌隔離乳<br>
							．淨白面膜<br>
							．光彩精華液<br>
							．保濕化妝水
						</p>
						<a href="mix-product-inner.php" class='link'> 
							<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								 viewBox="0 0 19 19" style="enable-background:new 0 0 19 19;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#231815;}
								.st1{fill:#FFFFFF;}
							</style>
							<g>
								<circle class="st0" cx="9.5" cy="9.5" r="9.5"/>
								<path class="st1" d="M15.7,10.7l-5,5c-0.6,0.6-1.7,0.6-2.4,0c-0.6-0.6-0.6-1.7,0-2.3c0.3-0.3,2.1-2.2,2.1-2.2h-6
									c-0.9,0-1.7-0.7-1.7-1.7s0.7-1.7,1.7-1.7h6c0,0-1.8-1.8-2.1-2.1C7.7,5,7.7,4,8.3,3.3s1.7-0.6,2.4,0l5,5C16.3,9,16.3,10,15.7,10.7z"
									/>
							</g>
							</svg>SEE MORE
						</a>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>



	<!-- banner3 -->
	<div class="row index-bn2 d-flex align-items-center" style="background-image: url('img/png/video_bg.jpg')">
		<div class="container" style="position: relative;">
			<div class="row d-flex align-items-center" style="margin: 20px;">

				<div class="col-12 col-md-6 push-md-6 bn3-box">
					<div>
						<h2 class='white'>抗皺景緻 對抗斑點</h2>
						<p class='white'>望度參，有有們……加現興！情此動已的的生，小為過一西日提竟動麼我進：我這發的教像轉放，綠結轉意信面：常真會例高樂企自是時弟費語等重溫新我電時要力；想氣著背形天時型府場……又二如密歡裡夫總首，的河所山裡臺河及於知境點每出藝些數，從麼解字：安界器良已年線寫傳生之溫！</p>
					</div>
				</div>


				<div class="col-12 col-md-6 pull-md-6">
					<div class="row">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ri212geRHQw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	<!-- Au product -->
	<div class="row ptb50">
		<div class="container">
			<div class="row">
				<div class="w-100">
					<h2 class='text-center lobster-font font40'>Au Products</h2>
				</div>
			</div>
			<div class="index-p3-out">
				<?php for($e=0;$e<3;$e++) { ?>
				<div class="index-p1">
					<div class="row">
						<?php for($i=0;$i<4;$i++) { ?>
						<div class="col-6 col-md-3" >
							<div class="ip-box">
								<a href="product-inner.php">
									<figure>
										<img src="img/png/p<?php echo $i%4+1 ;?>.jpg" class='img-rwd'>
										<?php if($i%3 == 0) { ?>
										<figcaption><p>限時<br>特價</p><span></span></figcaption>
										<?php } ?>
									</figure>
								</a>
								<h4 class='ip-name'>Cardamom 洗面乳</h4>
								<p class='ip-price-onsale'>特價：1200</p>
								<STRIKE class='ip-price'>原價：999</STRIKE>
								<a href="" class='ip-cart'>加入購物車</a>
							</div>
						</div>
						<?php } ?>
					
					</div>
				</div>
				<?php } ?>


			</div>
			<div class="row">
				<div class="col-12">
					<a href='product-all.php' class='seemore'>
						<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 viewBox="0 0 19 19" style="enable-background:new 0 0 19 19;" xml:space="preserve">
						<style type="text/css">
							.st0{fill:#231815;}
							.st1{fill:#FFFFFF;}
						</style>
						<g>
							<circle class="st0" cx="9.5" cy="9.5" r="9.5"/>
							<path class="st1" d="M15.7,10.7l-5,5c-0.6,0.6-1.7,0.6-2.4,0c-0.6-0.6-0.6-1.7,0-2.3c0.3-0.3,2.1-2.2,2.1-2.2h-6
								c-0.9,0-1.7-0.7-1.7-1.7s0.7-1.7,1.7-1.7h6c0,0-1.8-1.8-2.1-2.1C7.7,5,7.7,4,8.3,3.3s1.7-0.6,2.4,0l5,5C16.3,9,16.3,10,15.7,10.7z"
								/>
						</g>
						</svg>
						SEE MORE
					</a>
				</div>
			</div>
		</div>
	</div>


	<!-- slick3 -->
	<div class="row index-adslide">
		<div class='index-adslide-ul'>
			<?php for($i=0;$i<2;$i++){ ?>
			<div class="parent">
				<a href="news-inner.php">
					<div class="child"><img src="img/png/b_001.jpg" class="img-rwd"></div>
				</a>
			</div>
			<div class="parent">
				<a href="news-inner.php">
					<div class="child"><img src="img/png/b_002.jpg" class="img-rwd"></div>
				</a>
			</div>
			<div class="parent">
				<a href="news-inner.php">
					<div class="child"><img src="img/png/b_003.jpg" class="img-rwd"></div>
				</a>
			</div>
			<div class="parent">
				<a href="news-inner.php">
					<div class="child"><img src="img/png/b_004.jpg" class="img-rwd"></div>
				</a>
			</div>
			<?php } ?>
			<!-- <figure style=""><a href=""><img src="img/png/b_001.jpg" class='img-rwd' alt=""></a></figure> -->
			<!-- <figure><a href=""><img src="img/png/b_002.jpg" class='img-rwd' alt=""></a></figure>
			<figure><a href=""><img src="img/png/b_003.jpg" class='img-rwd' alt=""></a></figure>
			<figure><a href=""><img src="img/png/b_004.jpg" class='img-rwd' alt=""></a></figure> -->
		</div>
		<div class="index-control">
			<a class="left">
				<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"  viewBox="0 0 42 24" style="enable-background:new 0 0 42 24;" xml:space="preserve">
				
				<path d="M0,12l12,12h6L6,12L18,0l-6,0L0,12z M12,12l12,12h6L18,12L30,0l-6,0L12,12z M24,12l12,12h6L30,12L42,0l-6,0
					L24,12z"/>
				</svg>
			</a>
			<a class="right">
				<svg version="1.1" id="" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 42 24" style="enable-background:new 0 0 42 24;" xml:space="preserve">
				<path d="M30,0h-6l12,12L24,24h6l12-12L30,0z M18,0h-6l12,12L12,24h6l12-12L18,0z M6,0H0l12,12L0,24h6l12-12L6,0z"/>
				</svg>
			</a>
		</div>
	</div>
</div>



