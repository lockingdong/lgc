<form action="person-info.php" id="login-form">
	<div class="item-wrap">
		<label for="">帳號</label>
		<div class="input-wrap">
			<input name="email" type="text" value="" placeholder="輸入您的信箱">
		</div>
	</div>
	<div class="item-wrap">
		<label for="">密碼</label>
		<div class="input-wrap">
			<input name="pswd" type="password" value="" placeholder="輸入您的密碼">
		</div>
	</div>
	<div class="item-wrap">
		<label for="">
			<a class="forget-pswd-link" href="">忘記密碼</a>

		</label>
	</div>

	<div class="btn-area">
		<button type="submit">登入</button>
		<button class="fb-btn"><span class="fa fa-facebook"></span>使用Facebook登入</button>
	</div>
</form>