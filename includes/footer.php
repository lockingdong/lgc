<footer class="container-fluid">
	<div class="row">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-3">
					<h3>客服資訊</h3>
					<p>客服信箱:aaaa@aaa.aaa</p>
					<p>客服電話:04-2557-9237</p>
					<p>服務時間:AM09:00~PM5:00</p>
				</div>
				<div class="col-6 col-md-3 footer-link">
					<ul>
						<li>
							<a href="footer-terms.php">
								<img src="img/svg/btn_arrow_w.svg" class='svg-farrow'>付款方式
							</a>
						</li>
						<li>
							<a href="footer-terms.php">
								<img src="img/svg/btn_arrow_w.svg" class='svg-farrow'>運送方式
							</a>
						</li>
						<li>
							<a href="footer-terms.php">
								<img src="img/svg/btn_arrow_w.svg" class='svg-farrow'>退換貨方式
							</a>
						</li>
						<li>
							<a href="footer-terms.php">
								<img src="img/svg/btn_arrow_w.svg" class='svg-farrow'>常見問題
							</a>
						</li>
					</ul>	
				</div>
				<div class="col-6 col-md-3 footer-link">
					<ul>
						<li>
							<a href="footer-terms.php">
								<img src="img/svg/btn_arrow_w.svg" class='svg-farrow'>會員條款
							</a>
						</li>
						<li>
							<a href="footer-terms.php">
								<img src="img/svg/btn_arrow_w.svg" class='svg-farrow'>隱私權聲明
							</a>
						</li>
						<!-- <li>
							<a href="footer-terms.php">
								<img src="img/svg/btn_arrow_w.svg" class='svg-farrow'>退換貨方式
							</a>
						</li>
						<li>
							<a href="footer-terms.php">
								<img src="img/svg/btn_arrow_w.svg" class='svg-farrow'>常見問題
							</a>
						</li> -->
					</ul>	
				</div>
				<div class="col-12 col-md-3 footer-social">
					<ul>
						<li><a href=""><i class="fa fa-facebook"></i></a></li>
						<li><a href=""><i class="fa fa-instagram"></i></a></li>
						<li><a href=""><i class="fab fa-line" style="font-size: 28px;"></i></a></li>
						<!-- <li><a href=""><i class="fa fa-pinterest-p"></i></a></li> -->
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>


<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.17/vue.js"></script>
<script type="text/javascript" src='custom.js'></script>
</body>
</html>