<form action="verify-mail.php" id="register-form">
	<div class="item-wrap">
		<label for="">信箱</label>
		<div class="input-wrap">
			<input name="email" type="text" placeholder="輸入您的信箱">
		</div>
	</div>
	<div class="item-wrap">
		<label for="">密碼</label>
		<div class="input-wrap">
			<input id="pswd" name="pswd" type="password" placeholder="輸入您的密碼">
		</div>
	</div>
	<div class="item-wrap">
		<label for="">密碼確認</label>
		<div class="input-wrap">
			<input name="pswd-ag" type="password" placeholder="再次輸入您的密碼">
		</div>
	</div>
	<div class="item-wrap">
		<label for="">姓名</label>
		<div class="input-wrap">
			<input name="client-name" type="text" placeholder="輸入您的姓名">
		</div>
	</div>
	
	<div class="item-wrap">
		<label for="">手機</label>
		<div class="input-wrap">
			<input name="phone" type="text" placeholder="輸入您的手機號碼">
		</div>
	</div>
	<div class="item-wrap">
		<label for="">生日</label>
		<div class="input-wrap">
			<div class="date-wrap">
				<input class="placeholderclass" max="3000-12-31" name="birth" type="date" placeholder="輸入您的出生年月日" onClick="$(this).removeClass('placeholderclass')">
			</div>
		</div>
	</div>
	<div class="item-wrap">
		<label for="">地址</label>
		<div class="input-wrap">
			<div class="select-wrap">
				<select name="city">
					<option value="縣市" selected="" disabled>縣市</option>
					<option value="台北市">台北市</option>
				</select>
			</div>
			<div class="select-wrap">
				<select name="county">
					<option value="鄉鎮市區" selected="" disabled>鄉鎮市區</option>
					<option value="">中山區</option>
				</select>
			</div>
		</div>
	</div>
	<div class="item-wrap">
		<div class="label">
			<input name="zip" type="text" placeholder="XXX">
		</div>
		<div class="input-wrap">
			<input name="address" type="text" placeholder="輸入您的地址">
		</div>
	</div>
	<div class="checkbox-area">
		<input type="checkbox" id="check" name="checkterm">
		<label for="check">我已仔細閱讀並明瞭「會員服務條款」、「隱私權聲明」等所載內容及其意義</label>
	</div>
	<div class="btn-area apply-btn-area">
		<button type="submit">送出</button>
	</div>
</form>