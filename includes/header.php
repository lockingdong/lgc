<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Facebook
	================================================== -->
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:image" content="">
	<meta property="og:site_name" content="麗晶">
	<meta property="og:type" content="website">
	<meta property="og:locale" content="zh_TW">
	<meta property="og:url" content="http://moveon-design.com/love/">
	<!-- end Facebook
	================================================== -->


	<title>麗晶LEEJINN</title>
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">
	<!-- Favicons
	================================================== -->

	<!-- CSS
	================================================== -->

	<!-- <link rel="stylesheet" href="css/font-awesome.min.css"> -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/vendor/jqueryui/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="css/vendor/fontawesome/font-awesome.css">


	<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">	 -->
	<link rel="stylesheet" type="text/css" href="css/vendor/bootstrap/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/checkbox.min.css">

	<link rel="stylesheet" type="text/css" href="css/vendor/animation/animation.css">
	<link rel="stylesheet" type="text/css" href="css/vendor/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="css/vendor/slick/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/global.css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- CSS
	================================================== -->

	<style>
	.custom-badges {
		background-color: #516e51;
		border-radius: 50%;
		position: absolute;
		bottom: 85%;
		right: 0;
		color: #fff;
	}
	ul.navbar-user li {
		padding: 20px;
		
	}
	.navbar .navbar-user li svg path, .navbar .navbar-user li p {
		fill: #546E51;
		color: #546E51;
	}
	.navbar .navbar-user li:hover path, .navbar .navbar-user li:hover p {
		fill: #000;
		color: #000;
	}
	.header-input button {
		padding: 5px 15px;
	}
	.index-bn2 {
		cursor: pointer;
	}
	@media only screen and (max-width: 767px) {
		.index-article-box .index-article .intro {
			padding: 30px;
		}
	}
	@media only screen and (min-width: 768px) {
		.index-article-box .index-article .intro {
			padding: 50px;
		}
	}
	footer a .svg-farrow {
		width: 10px;
		margin-right: 5px;
	}
	.index-adslide-ul .parent:hover .child {
		transform: scale(1.2);
		transition: all .5s;
	}
	.child {
		height: 100%;
		width: 100%;
		background-size: cover;
		background-position: center;
		background-repeat: no-repeat;
		-webkit-transition: all .5s;
		transition: all .5s;
	}
	.parent {
		overflow: hidden;
	}
	.navbar .dropdown-menu .dropdown-submenu li ul li:hover {
		opacity: .7;
    	color: white;
	}
	</style>
	<!-- jQuery
	================================================== -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src='js/jquery-1.12.3.min.js'></script>
	<script src="js/vendor/slick/slick.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
	<script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>

	<!-- jQuery
	================================================== -->
	<!-- jQuery -->
</head>
<body>

<?php include('includes/navbar.php') ?>


