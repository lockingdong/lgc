<?php include 'includes/header.php'; ?>
<style>
    section.test {
    position: relative; }
    section.test .leaf {
        width: 100%;
        position: absolute;
        left: 0; }
    section.test .leaf.top {
        top: 0;
        -webkit-transform: rotate(180deg) translateY(10%);
        -ms-transform: rotate(180deg) translateY(10%);
        transform: rotate(180deg) translateY(10%); }
    section.test .leaf.bottom {
        bottom: 0;
        -webkit-transform: translateY(10%);
        -ms-transform: translateY(10%);
        transform: translateY(10%); }
    section.test .wrap {
        padding: 800px 0; }
        @media (max-width: 1199px) {
        section.test .wrap {
            padding: 500px 0; } }
        @media (max-width: 768px) {
        section.test .wrap {
            padding: 400px 0; } }
        @media (max-width: 576px) {
        section.test .wrap {
            padding: 300px 0; } }
        @media (max-width: 400px) {
        section.test .wrap {
            padding: 400px 0; } }
        section.test .wrap .logo-wrap {
        text-align: center;
        margin-bottom: 100px;
        position: relative; }
        section.test .wrap .logo-wrap:before {
            content: '';
            position: absolute;
            width: 50px;
            background: #bbb;
            height: 2px;
            left: 50%;
            -webkit-transform: translateX(-50%);
            -ms-transform: translateX(-50%);
            transform: translateX(-50%);
            bottom: -50px; }
        section.test .wrap .logo-wrap img {
            width: 30%;
            min-width: 180px; }
        section.test .wrap h1 {
        text-align: center;
        font-weight: 300;
        color: #3f533c; }

    body {
        background: #e5e4e2;
    }
    header {
        display: none;
    }
    .header-input {
        display: none;
    }
</style>
        <section class="test">
            <img class="leaf top" src="img/footer-terms/leaf.jpg" alt="">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="wrap">

                            <div class="leaf top"></div>
                            <div class="logo-wrap">
                                <img src="img/svg/logo-01.svg">
                            </div>
                            <h1>
                                網站建置中<br>
                                敬請期待
                            </h1>      
                        </div>
                    </div>
                </div>  
            </div>
            <img class="leaf bottom" src="img/footer-terms/leaf.jpg" alt="">
        </section>
    </body>
</html>
