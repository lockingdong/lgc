<?php include 'includes/header.php'; ?>

<section id="person-login-register" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>Login or register</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="info-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12 col-xl-6">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>會員登入</div>
					</div>	
					<div class="form-wrap">
						<?php include 'includes/login-form.php'; ?>
					</div>
				</div>
				<div class="col-12 col-xl-6">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>新用戶註冊</div>
					</div>	
					<div class="form-wrap">
						<?php include 'includes/register-form.php'; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="mask" style="opacity: 0"></div>
<div class="forget-pswd-page forget info-bottom" style="opacity: 0">
	<div class="form-wrap">
		<div class="title">
			<span>忘記密碼</span>
			<div class="close-btn">✕</div>
		</div>
		<?php include 'includes/forget-pswd-form.php'; ?>
	</div>
</div>




<?php include 'includes/footer.php'; ?>
<script>
	$(".forget-pswd-page.forget, .mask").css("display", "none");
</script>