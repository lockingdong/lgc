<?php include 'includes/header.php'; ?>

<section id="person-login-register" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>Login or register</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="info-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12 col-xl-6">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>會員登入</div>
					</div>	
					<div class="form-wrap">
						<?php include 'includes/login-form.php'; ?>
					</div>
				</div>
				<div class="col-12 col-xl-6">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>新用戶註冊</div>
					</div>	
					<div class="form-wrap">
						<?php include 'includes/register-form.php'; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="mask"></div>
<div class="forget-pswd-page verify info-bottom">
	<div class="form-wrap">
		<div class="title">
			<span>驗證信已寄出</span>
			<div class="close-btn">✕</div>
		</div>
		<div class="content">
			<div class="svg">
				<img src="img/svg/mail-icon.svg" alt="">
			</div>
			<p>請前往您的信箱打開它，並完成最後步驟！</p>
			
		</div>
		<div class="btn-area">
			<button type="submit" class="close-btn">確定</button>
		</div>
	</div>
</div>
<div class="forget-pswd-page forget info-bottom">
	<div class="form-wrap">
		<div class="title">
			<span>忘記密碼</span>
			<div class="close-btn">✕</div>
		</div>
		<?php include 'includes/forget-pswd-form.php'; ?>
	</div>
</div>




<?php include 'includes/footer.php'; ?>
<script>
	$(".forget-pswd-page.verify, .mask").css("display", "block");
	$("header, .header-input, section, footer").addClass("blur-class");
</script>