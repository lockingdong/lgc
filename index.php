<?php 

include 'includes/header.php';

if(isset($_GET['page'])){
	$page = $_GET['page'];
}else{
	$page  = '';
}

switch ($page) {
	case 'contact':
		include 'includes/contact.php';
		break;

	case 'current_list':
		include 'includes/current_list.php';
		break;
	case 'current':
		include 'includes/current.php';
		break;


	case 'news_list':
		include 'includes/news_list.php';
		break;
	case 'news':
		include 'includes/news.php';
		break;

	case 'product':
		include 'includes/product.php';
		break;
	case 'about':
		include 'includes/about.php';
		break;

	default:
		include 'includes/home.php';
		break;
}


	
include 'includes/footer.php'; ?>


