<?php include 'includes/header.php'; ?>

<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="banner-wrap">
					<img src="img/about/pic/01.jpg" alt="">
				</div>
			</div>
		</div>
	</div>
	<div class="container container-p">
		<div class="row">
			<div class="col-lg-3 col-12">
				<div class="title green-decor-title">
					<div class="decor-squ"></div>
					<div>品牌故事</div>
				</div>
			</div>
			<div class="col-lg-7 col-12">
				<div class="content-wrap">
					<p>
						太平洋發跡於1956年，飄洋過海、聲名遠播，同時在美容業界交出了亮眼的成績單。隨著時代潮流轉變，我們轉型幕後，服務並經歷更多不同領域的客戶，累績雄厚的經驗，不斷創新、突破自我。對太平洋而言，這不僅僅是個品牌，而是一個情感的傳承，我們堅信且堅持大自然中最純淨、天然的能量，才是帶給人們最寧靜的享受，創造浩大、寬廣、無限的力量。
					</p>
				</div>
			</div>
		</div>
	</div>
	<div class="middle-text">
		太平洋好堅持，因為用心、堅持、相信、感恩，經歷半世紀，好完整
	</div>
	<div class="container container-p">
		<div class="row">
			<div class="col-12">
				<div class="title green-decor-title">
					<div class="decor-squ"></div>
					<div>品牌故事</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-6 col-lg-3">
				<div class="svg-text-area">
					<div class="img-wrap">
						<img src="img/about/svg/icon-01.svg" alt="">
					</div>
					<div class="title-area">
						<p>原創性</p>
						<p>Originality</p>
					</div>
					<div class="text-wrap">
						<p>所有產品由專屬研發團隊主導開發配方，最了解產的成本和功效，為寵愛自己的你，嚴格把關，「因為我們用心，讓您安心」。</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-3">
				<div class="svg-text-area">
					<div class="img-wrap">
						<img src="img/about/svg/icon-02.svg" alt="">
					</div>
					<div class="title-area">
						<p>安全和健康</p>
						<p>Safety Healthy</p>
					</div>
					<div class="text-wrap">
						<p>認為好的成份和產品不是依外觀好看來決定，我們嚴選成份，堅持純淨、溫和且有效品質，由植物香氣開始照顧您的感官，「因為我們用心，讓您信賴」。</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-3">
				<div class="svg-text-area">
					<div class="img-wrap">
						<img src="img/about/svg/icon-03.svg" alt="">
					</div>
					<div class="title-area">
						<p>品質至上</p>
						<p>High Quality Goods</p>
					</div>
					<div class="text-wrap">
						<p>認為好的成份和產品不是依外觀好看來決定，我們嚴選成份，堅持純淨、溫和且有效品質，由植物香氣開始照顧您的感官，「因為我們用心，讓您信賴」。</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-3">
				<div class="svg-text-area">
					<div class="img-wrap">
						<img src="img/about/svg/icon-04.svg" alt="">
					</div>
					<div class="title-area">
						<p>回饋和信念</p>
						<p>Reward And Faith</p>
					</div>
					<div class="text-wrap">
						<p>認為好的成份和產品不是依外觀好看來決定，我們嚴選成份，堅持純淨、溫和且有效品質，由植物香氣開始照顧您的感官，「因為我們用心，讓您信賴」。</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<?php include 'includes/footer.php'; ?>