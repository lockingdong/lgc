<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Facebook
	================================================== -->
	<meta property="og:title" content="">
	<meta property="og:description" content="">
	<meta property="og:image" content="">
	<meta property="og:site_name" content="麗晶">
	<meta property="og:type" content="website">
	<meta property="og:locale" content="zh_TW">
	<meta property="og:url" content="http://moveon-design.com/love/">
	<!-- end Facebook
	================================================== -->


	<title>麗晶LEEJINN</title>
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">
	<!-- Favicons
	================================================== -->

	<!-- CSS
	================================================== -->

	<link rel="stylesheet" type="text/css" href="css/vendor/jqueryui/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="css/vendor/fontawesome/font-awesome.css">

	<link rel="stylesheet" type="text/css" href="css/vendor/bootstrap/bootstrap.css">

	
	<link rel="stylesheet" type="text/css" href="css/vendor/animation/animation.css">
	<link rel="stylesheet" type="text/css" href="css/vendor/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="css/vendor/slick/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- CSS
	================================================== -->


	<!-- jQuery
	================================================== -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src='js/jquery-1.12.3.min.js'></script>
	<script src="js/vendor/slick/slick.min.js"></script>

	<!-- jQuery
	================================================== -->
	<!-- jQuery -->
</head>
<body>
<?php $page = $_GET['page'];?>

<div class="container-fluid">

	<?php 
	switch ($page) {
		case 'about':
		echo "<img src='img/page/about.jpg' class='img-rwd'>";
		break;

		case 'contact':
		echo "<img src='img/page/contact.jpg' class='img-rwd'>";
		break;

		case 'current':
		echo "<img src='img/page/current.jpg' class='img-rwd'>";
		break;

		case 'current-inside':
		echo "<img src='img/page/current-inside.jpg' class='img-rwd'>";
		break;

		case 'product':
		echo "<img src='img/page/product.jpg' class='img-rwd'>";
		break;

		case 'news':
		echo "<img src='img/page/news.jpg' class='img-rwd'>";
		break;

		case 'news-inside':
		echo "<img src='img/page/news-inside.jpg' class='img-rwd'>";
		break;


		default:
			# code...
			break;
	}
	?>
			
		
</div>
</body>
</html>

