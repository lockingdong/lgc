<?php include 'includes/header.php'; ?>

<section id="mix-product">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="main-title">
					Combination product
				</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12 col-md-6 col-lg-12">
				<div class="bottom-wrap">
					<div class="row">
						<div class="col-12 col-xl-7">
							<div class="img-wrap">
								<img src="img/mix-product/c_001.jpg" alt="">
							</div>
						</div>
						<div class="col-12 col-xl-5">
							<div class="text-wrap">
								<div class="product-intro">
									<h1>華麗美顏組合</h1>
									<h3>特價：2399元</h3>
									<div class="content">
										<p>液狀質地滲透洗面乳</p>
										<p>淨白面膜</p>
										<p>光采精華乳液</p>
										<p>保濕化妝水</p>
									</div>
								</div>
								<div class="see-more-area">
									<a href="mix-product-inner.php">
										<button><img src="img/svg/btn_arrow.svg" alt="">SEE MORE</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-12">
				<div class="bottom-wrap">
					<div class="row">
						<div class="col-12 col-xl-7">
							<div class="img-wrap">
								<img src="img/mix-product/c_002.jpg" alt="">
							</div>
						</div>
						<div class="col-12 col-xl-5">
							<div class="text-wrap">
								<div class="product-intro">
									<h1>華麗美顏組合</h1>
									<h3>特價：2399元</h3>
									<div class="content">
										<p>液狀質地滲透洗面乳</p>
										<p>淨白面膜</p>
										<p>光采精華乳液</p>
										<p>保濕化妝水</p>
									</div>
								</div>
								<div class="see-more-area">
									<a href="mix-product-inner.php">
										<button><img src="img/svg/btn_arrow.svg" alt="">SEE MORE</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-12">
				<div class="bottom-wrap">
					<div class="row">
						<div class="col-12 col-xl-7">
							<div class="img-wrap">
								<img src="img/mix-product/c_003.jpg" alt="">
							</div>
						</div>
						<div class="col-12 col-xl-5">
							<div class="text-wrap">
								<div class="product-intro">
									<h1>華麗美顏組合</h1>
									<h3>特價：2399元</h3>
									<div class="content">
										<p>液狀質地滲透洗面乳</p>
										<p>淨白面膜</p>
										<p>光采精華乳液</p>
										<p>保濕化妝水</p>
									</div>
								</div>
								<div class="see-more-area">
									<a href="mix-product-inner.php">
										<button><img src="img/svg/btn_arrow.svg" alt="">SEE MORE</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-12">
				<div class="bottom-wrap">
					<div class="row">
						<div class="col-12 col-xl-7">
							<div class="img-wrap">
								<img src="img/mix-product/c_001.jpg" alt="">
							</div>
						</div>
						<div class="col-12 col-xl-5">
							<div class="text-wrap">
								<div class="product-intro">
									<h1>華麗美顏組合</h1>
									<h3>特價：2399元</h3>
									<div class="content">
										<p>液狀質地滲透洗面乳</p>
										<p>淨白面膜</p>
										<p>光采精華乳液</p>
										<p>保濕化妝水</p>
									</div>
								</div>
								<div class="see-more-area">
									<a href="mix-product-inner.php">
										<button><img src="img/svg/btn_arrow.svg" alt="">SEE MORE</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-12">
				<div class="bottom-wrap">
					<div class="row">
						<div class="col-12 col-xl-7">
							<div class="img-wrap">
								<img src="img/mix-product/c_002.jpg" alt="">
							</div>
						</div>
						<div class="col-12 col-xl-5">
							<div class="text-wrap">
								<div class="product-intro">
									<h1>華麗美顏組合</h1>
									<h3>特價：2399元</h3>
									<div class="content">
										<p>液狀質地滲透洗面乳</p>
										<p>淨白面膜</p>
										<p>光采精華乳液</p>
										<p>保濕化妝水</p>
									</div>
								</div>
								<div class="see-more-area">
									<a href="mix-product-inner.php">
										<button><img src="img/svg/btn_arrow.svg" alt="">SEE MORE</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-12">
				<div class="bottom-wrap">
					<div class="row">
						<div class="col-12 col-xl-7">
							<div class="img-wrap">
								<img src="img/mix-product/c_003.jpg" alt="">
							</div>
						</div>
						<div class="col-12 col-xl-5">
							<div class="text-wrap">
								<div class="product-intro">
									<h1>華麗美顏組合</h1>
									<h3>特價：2399元</h3>
									<div class="content">
										<p>液狀質地滲透洗面乳</p>
										<p>淨白面膜</p>
										<p>光采精華乳液</p>
										<p>保濕化妝水</p>
									</div>
								</div>
								<div class="see-more-area">
									<a href="mix-product-inner.php">
										<button><img src="img/svg/btn_arrow.svg" alt="">SEE MORE</button>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="pagination">
					<ul>
						<li><a href="">1</a></li>
						<li><a href="">2</a></li>
						<li><a href="">3</a></li>
						<li><a href="">></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>



<script>
	$("body").css({
		"background-image": "url('../lgc/img/mix-product/bg.png')",
		//"background-size": "cover"
	});
	$(".bottom-wrap:odd").find(".col-12.col-xl-7").addClass("order-xl-7")
</script>
<?php include 'includes/footer.php'; ?>