<?php include 'includes/header.php'; ?>

<section id="mix-product-inner">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6">
				<div class="product-img">
					<div class="img-wrap">
						<img src="img/product-inner/01.jpg" alt="">					
					</div>	
				</div>
				<div class="img-slider-wrap">
					<div class="slider img-slider">
            <li>
            	<div class="img-wrap">
            		<img src="img/product-inner/01.jpg" alt="">	
            	</div>
            </li>
            <li>
            	<div class="img-wrap">
            		<img src="img/product-inner/image.jpg" alt="">	
            	</div>
            </li>
            <li>
            	<div class="img-wrap">
            		<img src="img/product-inner/image.jpg" alt="">	
            	</div>
            </li>
            <li>
            	<div class="img-wrap">
            		<img src="img/product-inner/image.jpg" alt="">	
            	</div>
            </li>
            <li>
            	<div class="img-wrap">
            		<img src="img/product-inner/image.jpg" alt="">	
            	</div>
            </li>
          </div>
				</div>
			</div>
			<div class="col-12 col-lg-6">
				<!-- <div class="container"> -->
					<div class="row">
						<div class="col-12">
							<div class="product-title">
								<div class="main-title">華麗保養組合</div>
								<div class="sub-title">高濃度氨基酸</div>
							</div>
						</div>
						
					</div>
				<!-- </div> -->
				
				<div class="product-text">
					<!-- <div class="container"> -->
						<div class="row">
							<div class="col-12">
								<p>我趕緊拭乾了淚，怕他看見，也怕別人看見。我再向外看時，他已抱 了朱紅的橘子往回走了。過鐵道時，他先將橘子散放在地上，自己慢慢爬下，再抱起橘子走。到這邊時，我趕緊去攙他。他和我走到車上 ，將橘子一股腦兒放在我的皮大衣上。
								</p>
							</div>
						</div>
						<hr>
					<!-- </div> -->
					
				</div>
				<div class="product-form-wrap">
					<form action="">
						<!-- <div class="container"> -->
							<div class="row">

								<div class="col-12 col-lg-8">
									<div class="item">
										<label for="">皮膚類型</label>
										<span>全膚質（中油、暗瘡尤佳）</span>
									</div>
									<div class="item">
										<label for="">數量</label>
										<div class="select-wrap">
											<select name="" id="">
												<option value="">1</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-12 col-lg-4">
									<div class="add-to-cart">
										<input type="submit" value="加入購物車">
									</div>
								</div>
									
							</div>
						<!-- </div> -->
						
					</form>
				</div>	
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<!-- <div class="col-12">
				<div class="mix-product-title">
					<h1>透過專業選才</h1>
					<h1>為您提出華麗清潔方案</h1>
					<hr>
					<h3>以下是EDM或其他圖文製作須知</h3>
				</div>
			</div> -->
			<div class="col-12">
				<div class="edm-img">
					<img src="img/mix-product-inner/EDM.jpg" alt="">
				</div>
				<div class="text-area">
					<h4>此區域也能用打字的方式編輯</h4>
					<p>
						過鐵道時，他先將橘子散放在地上，自己慢慢爬下，再抱起橘子走。到這邊時，我趕緊去攙他。他和我走到車上 ，將橘子一股腦兒放在我的皮大衣上。於是拍拍衣上泥土，心裡很輕鬆似的。過一會說，「我走了，到那邊來信！」，我望著他走出去。他走了幾步，回過頭看見我，說，「進去吧，裡邊沒人。」等他的背影混入來來往往的人裡，再找不著了，我便進來坐下，我的眼淚又來了。
					</p>
				</div>


				<div class="insert-img-area">
					<h4>並插入圖片</h4>
					<div class="insert-img">
						<img src="img/mix-product-inner/01.jpg" alt="">
					</div>
					<p>圖片寬度儘量統一、一置 畫面才會比較整齊喔</p>
					<div class="insert-img">
						<img src="img/mix-product-inner/02.jpg" alt="">
					</div>
				</div>



			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="item-wrap">
					<div class="row">
						<div class="col-12 col-lg-3">
							<div class="title green-decor-title">
								<div class="decor-squ"></div>
								<div>內容</div>
							</div>
						</div>
						<div class="col-12 col-lg-9">
							<div class="intro-text">
								<p>
									1.我趕緊拭乾了淚，怕他看見，也怕別人看見。<br>
									2.我再向外看時，他已抱 了朱紅的橘子往回走了。
								</p>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-12">
				<div class="item-wrap">
					<div class="row">
						<div class="col-12 col-lg-3">
							<div class="title green-decor-title">
								<div class="decor-squ"></div>
								<div>使用方法</div>
							</div>
						</div>
						<div class="col-12 col-lg-9">
							<div class="intro-text">
								<p>
									1.我趕緊拭乾了淚，怕他看見，也怕別人看見。<br>
									2.我再向外看時，他已抱 了朱紅的橘子往回走了。
								</p>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-12">
				<div class="item-wrap">
					<div class="row">
						<div class="col-12 col-lg-3">
							<div class="title green-decor-title">
								<div class="decor-squ"></div>
								<div>成份</div>
							</div>
						</div>
						<div class="col-12 col-lg-9">
							<div class="intro-text">
								<p>
									過鐵道時，他先將橘子散放在地上，自己慢慢爬下，再抱起橘子走。到這邊時，我趕緊去攙他。他和我走到車上，將橘子一股腦兒放在我的皮大衣上。於是拍拍衣上泥土，心裡很輕鬆似的。過一會說，「我走了，到那邊來信！」，我望著他走出去。他走了幾步，回過頭看見我，說，「進去吧，裡邊沒人。」等他的背影混入來來往往的人裡，再找不著了，我便進來坐下，我的眼淚又來了。
								</p>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-12">
				<div class="item-wrap">
					<div class="row">
						<div class="col-12 col-lg-3">
							<div class="title green-decor-title">
								<div class="decor-squ"></div>
								<div>使用注意事項</div>
							</div>
						</div>
						<div class="col-12 col-lg-9">
							<div class="intro-text">
								<p>
									我趕緊拭乾了淚，怕他看見，也怕別人看見。<br>
									我再向外看時，他已抱 了朱紅的橘子往回走了。<br>
									過鐵道時，他先將橘子散放在地上，自己慢 <br>慢爬下，再抱起橘子走。到這邊時，我趕緊去攙他。他和我走到車上 <br>，將橘子一股腦兒放在我的皮大衣上。
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<?php include 'includes/footer.php'; ?>