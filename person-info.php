<?php include 'includes/header.php'; ?>

<section id="person-info" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>Personal information</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="info-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>個人資訊</div>
					</div>	
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-6">
					<div class="form-wrap">
						<form id="person-info-form" action="">
							<div class="item-wrap">
								<label for="">帳號</label>
								<div class="input-wrap">
									<span>aaaaaaaaa@gmail.com</span>
								</div>
							</div>
							<div class="item-wrap">
								<label for="">姓名</label>
								<div class="input-wrap">
									<input name="client-name" type="text" value="阿明">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">密碼</label>
								<div class="input-wrap">
									<span><a href="reset-pswd.php">設定新密碼</a></span>
								</div>
							</div>
							<div class="item-wrap">
								<label for="">生日</label>
								<div class="input-wrap">
									<div class="date-wrap">
										<input class="placeholderclass" max="3000-12-31" name="birth" type="date" placeholder="輸入您的出生年月日" onClick="$(this).removeClass('placeholderclass')">
									</div>
								</div>
							</div>
							<div class="item-wrap">
								<label for="">手機</label>
									<div class="input-wrap">
										<input name="phone" type="text" value="0900123456">
									</div>
								</div>
							<div class="item-wrap">
								<label for="">地址</label>
								<div class="input-wrap">
									<div class="select-wrap">
										<select name="" id="">
											<option value="">台北市</option>
										</select>
									</div>
									<div class="select-wrap">
										<select name="" id="">
											<option value="">中山區</option>
										</select>
									</div>
								</div>
							</div>
							<div class="item-wrap">
								<div class="label">
									<input type="text" value="103">
								</div>
								<div class="input-wrap">
									<input type="text" value="長安東路二段142號">
								</div>
							</div>
							<div class="btn-area">
								<button type="submit">儲存變更</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<?php include 'includes/footer.php'; ?>