<?php include 'includes/header.php'; ?>


<section id="order" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>Order</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="order-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-6">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>訂單2017123107361041</div>
					</div>	
				</div>
				<div class="col-12 col-lg-3">
					<div class="status">
						狀態：已完成
					</div>
				</div>
				<div class="col-12 col-lg-3">
					<div class="date">
						日期：2017-12-31
					</div>	
				</div>
			</div>
			<div class="list-title">
				<div class="row">
					<div class="col-4">商品</div>
					<div class="col-3">單價</div>
					<div class="col-3">數量</div>
					<div class="col-2">小計</div>
				</div>	
			</div>
			<div class="list-wrap">
				<?php
					$product = array(
						"超速修護安瓶組基因高活性凝凍面膜NEW", 
						"超速修護安瓶組", 
						"基因高活性凝凍面膜NEW", 
						"絕對完美極淨鑽白CC霜SPF50/PA+++", 
						"超速修護安瓶組", 
						"基因高活性凝凍面膜NEW");
					$price = array(
						1200,
						900,
						1000,
						1200,
						900,
						1000
					);
					$qua = array(
						1,2,1,1,1,3
					);
					$i=0; 
					while($i<6): 
				?>
				<div class="list">
					<div class="row">
						<div class="col-12 col-lg-4">
							<span class="item-title">商品&nbsp</span><span><?php echo $product[$i]; ?></span>
						</div>
						<div class="col-12 col-lg-3">
							<span class="item-title">單價&nbsp</span><span><?php echo $price[$i]; ?></span>
						</div>
						<div class="col-12 col-lg-3">
							<span class="item-title">數量&nbsp</span><span><?php echo $qua[$i]; ?></span>
						</div>
						<div class="col-12 col-lg-2">
							<span class="item-title">小計&nbsp</span>
							<span>NT$<?php echo $price[$i]*$qua[$i]; ?></span>
						</div>
					</div>	
				</div>
				<?php 
					$i++;
					endwhile; 
				?>
			</div>
			<div class="total-area">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="total">
								<div class="total-list">
									<div class="item"><span>小計</span><span>NT$9,100</span></div>
									<div class="item"><span>運費</span><span>NT$100</span></div>
									<div class="item"><span>總計</span><span>NT$9,200</span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
		<hr>
		<div class="list-bottom">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6">
						<div class="text-wrap">
							<div class="bt-title">收件人資訊</div>
							<div class="item-wrap"><span>姓名：</span><span>王小明</span></div>
							<div class="item-wrap"><span>電話：</span><span>09999999**</span></div>
						</div>
					</div>
					<div class="col-12 col-lg-6">
						<div class="text-wrap">
							<div class="bt-title">付款資訊</div>
							<div class="item-wrap"><span>付款方式：</span><span>7-11取貨付款（限本島）</span></div>
							<div class="item-wrap"><span>付款狀態：</span><span>未付款</span></div>
						</div>
					</div>
					<div class="col-12">
						<div class="text-wrap">
							<div class="bt-title">付款資訊</div>
							<div class="item-wrap">
								<span>送貨方式：</span>
								<span>7-11取貨付款<a href=""><button class="track-btn">7-11物流追蹤</button></a></span>
							</div>
							<div class="item-wrap"><span>送貨狀態：</span><span>處理中</span></div>
							<div class="item-wrap"><span>7-11店號：</span><span>123456</span></div>
							<div class="item-wrap"><span>7-11門市名：</span><span>某某路門市</span></div>
							<div class="item-wrap"><span>配送編號：</span><span>1234567890123</span></div>
							<div class="item-wrap"><span>門市地址：</span><span>台北市中正區重慶南路一段122號</span></div>
						</div>	
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="prev-btn">
							<a href="order-list.php"><button>回上一頁</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<?php include 'includes/footer.php'; ?>