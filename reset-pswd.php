<?php include 'includes/header.php'; ?>

<section id="reset-password" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>reset Password</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="info-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					
					<div class="form-wrap">
						<form id="reset-pswd" action="reset-pswd-suc.php">
							<div class="item-wrap">
								<label for="">請輸入新密碼</label>
								<div class="input-wrap">
									<input type="password" placeholder="" name="pswd" id="pswd">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">再次確認密碼</label>
								<div class="input-wrap">
									<input type="password" placeholder="" name="pswd-ag">
								</div>
							</div>
							<div class="btn-area apply-btn-area">
								<button type="submit">送出</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>





<?php include 'includes/footer.php'; ?>