<?php include 'includes/header.php'; ?>

<section id="person-login-register" class="top-bottom-empty">
	<div class="main-title-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="main-title">
						<h1>Login or register</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="info-bottom">
		<div class="container">
			<div class="row">
				<div class="col-12 col-xl-6">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>會員登入</div>
					</div>	
					<div class="form-wrap">
						<form action="">
							<div class="item-wrap">
								<label for="">帳號</label>
								<div class="input-wrap">
									<input type="text" value="" placeholder="輸入您的信箱">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">密碼</label>
								<div class="input-wrap">
									<input type="password" value="" placeholder="輸入您的密碼">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">
									<a class="forget-pswd-link" href="">忘記密碼</a>
				
								</label>
							</div>

							<div class="btn-area">
								<button type="submit">登入</button>
								<button class="fb-btn"><span class="fa fa-facebook"></span>使用Facebook登入</button>
							</div>
						</form>
					</div>
				</div>
				<div class="col-12 col-xl-6">
					<div class="title green-decor-title">
						<div class="decor-squ"></div>
						<div>新用戶註冊</div>
					</div>	
					<div class="form-wrap">
						<form action="">
							<div class="item-wrap">
								<label for="">信箱</label>
								<div class="input-wrap">
									<input type="text" placeholder="輸入您的信箱">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">密碼</label>
								<div class="input-wrap">
									<input type="text" placeholder="輸入您的密碼">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">密碼確認</label>
								<div class="input-wrap">
									<input type="text" placeholder="再次輸入您的密碼">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">姓名</label>
								<div class="input-wrap">
									<input type="text" placeholder="輸入您的姓名">
								</div>
							</div>
							
							<div class="item-wrap">
								<label for="">手機</label>
								<div class="input-wrap">
									<input type="text" placeholder="輸入您的手機號碼">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">生日</label>
								<div class="input-wrap">
									<input type="text" placeholder="輸入您的出生年月日">
								</div>
							</div>
							<div class="item-wrap">
								<label for="">地址</label>
								<div class="input-wrap">
									<div class="select-wrap">
										<select>
											<option value="縣市" selected="">縣市</option>
											<option value="台北市">台北市</option>
										</select>
									</div>
									<div class="select-wrap">
										<select>
											<option value="鄉鎮市區" selected="">鄉鎮市區</option>
											<option value="">中山區</option>
										</select>
									</div>
								</div>
							</div>
							<div class="item-wrap">
								<div class="label">
									<input type="text" placeholder="XXX">
								</div>
								<div class="input-wrap">
									<input type="text" placeholder="輸入您的地址">
								</div>
							</div>
							<div class="checkbox-area">
								<input type="checkbox" id="check">
								<label for="check">我已仔細閱讀並明瞭「會員服務條款」、「隱私權聲明」等所載內容及其意義</label>
							</div>
							<div class="btn-area apply-btn-area">
								<button type="submit">送出</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="mask"></div>
<div class="forget-pswd-page info-bottom">
	<div class="form-wrap">
		<div class="title">
			<span>完成</span>
			<a href="person-login-register.php">
				<div class="close-btn">✕</div>
			</a>
			
		</div>
		<div class="content">
			<!-- <p>2342352352353253252345235242342342</p> -->
			<p>您已完成修改密碼，請重新登入</p>
		</div>
		<div class="btn-area">
			<a href="person-login-register.php">
				<button type="submit" class="close-btn">登入</button>
			</a>
		</div>
	</div>
</div>




<?php include 'includes/footer.php'; ?>
<script>
	$(".forget-pswd-page, .mask").css("display", "block");
	$("header, .header-input, section, footer").addClass("blur-class");
</script>